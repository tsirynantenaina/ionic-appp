import { Component, OnInit } from '@angular/core';
import { ProduitService} from 'src/app/services/produit.service';
import { AchatService} from 'src/app/services/achat.service';
import {LoadingController , ToastController} from '@ionic/angular';


@Component({
  selector: 'app-achat',
  templateUrl: './achat.page.html',
  styleUrls: ['./achat.page.scss'],
})
export class AchatPage implements OnInit {
 listeProduit: any ;
 sommeData: any ; 
 somme:number=0 ;
  constructor(
      private achatService: AchatService,
      private produitService: ProduitService,
      private loadingCtrl:LoadingController , 
      private toastController: ToastController  ) { }

  ngOnInit() {
  }

  ionViewDidEnter(){
    this.informationPanier();
    this.allProduit();
  }

  async allProduit(){
    const loading = await this.loadingCtrl.create({message: 'Loading...'});
    loading.present();
    this.produitService.getAllProduit().subscribe(
       res =>{this.listeProduit= res ; loading.dismiss() ; console.log(this.listeProduit)}
    );
  }


  informationPanier(){
    this.achatService.informationPanier().subscribe(res => {
            this.sommeData = res ;
            this.somme = this.sommeData.somme ; 
            console.log(res) ; 
    });
  }



}
