import { Component, OnInit } from '@angular/core';
import { AchatService} from 'src/app/services/achat.service';
import { ProduitService} from 'src/app/services/produit.service';
import {ActivatedRoute ,Router} from '@angular/router';

import {LoadingController , AlertController , ToastController} from '@ionic/angular';
import {FormControl , FormGroup , Validators} from '@angular/forms'

@Component({
  selector: 'app-add-panier',
  templateUrl: './add-panier.page.html',
  styleUrls: ['./add-panier.page.scss'],
})
export class AddPanierPage implements OnInit {
  id:number=0;
  produitData : any ; 
  designation:string="";
  nomFrns: string ="";
  stock: number;
  prix:number;
  formPanier:FormGroup;

  
  sommeData:any ;
  somme:number=0 ; 

  constructor(
    private achatService: AchatService,
    private produitService: ProduitService,
    private loadingController: LoadingController, 
    private toastController: ToastController , 
    private route: ActivatedRoute , 
    private router: Router ) { }

  ngOnInit() {
    this.initialiseForm();
    if(this.route.snapshot.params.id){
      this.id=this.route.snapshot.params.id;
      this.getProduitById(this.id);
      
    };
  }

  //get valeur à modifier  
  async getProduitById(id:number){
    const loading = await this.loadingController.create({message: 'Loading...'});
    loading.present();
      this.produitService.getProduitById(id).subscribe( res=>{
          this.produitData = res;   
          this.designation = this.produitData.produit.designation ;
          this.nomFrns = this.produitData.produit.nomFrns; 
          this.stock = this.produitData.produit.quantite;    
          this.prix = this.produitData.produit.prix;   
          loading.dismiss();
      });  
  }

  initialiseForm(){
    this.formPanier = new FormGroup({
        quantiteAchat: new FormControl(null , [Validators.required]),
    });
  }

   addPanier(){
    this.achatService.addPanier(this.id , this.formPanier.value).subscribe(res => {
        this.showAlert('Produit ajouter dans panier');
        this.router.navigate(['/achat']);  
    });
  }

   //alert();
  async showAlert(MyMessage:string){
    const toast= await this.toastController.create({
      message: ''+MyMessage+'',
      duration: 4000,
      position: 'bottom', 
    });
    toast.present();
  }



}
