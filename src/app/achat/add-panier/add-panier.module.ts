import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddPanierPageRoutingModule } from './add-panier-routing.module';

import { AddPanierPage } from './add-panier.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
     ReactiveFormsModule,
    AddPanierPageRoutingModule
  ],
  declarations: [AddPanierPage]
})
export class AddPanierPageModule {}
