import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddPanierPage } from './add-panier.page';

const routes: Routes = [
  {
    path: '',
    component: AddPanierPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddPanierPageRoutingModule {}
