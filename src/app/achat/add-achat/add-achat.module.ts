import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddAchatPageRoutingModule } from './add-achat-routing.module';

import { AddAchatPage } from './add-achat.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddAchatPageRoutingModule
  ],
  declarations: [AddAchatPage]
})
export class AddAchatPageModule {}
