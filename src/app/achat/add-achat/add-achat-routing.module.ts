import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddAchatPage } from './add-achat.page';

const routes: Routes = [
  {
    path: '',
    component: AddAchatPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddAchatPageRoutingModule {}
