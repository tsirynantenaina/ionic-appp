import { Component, OnInit } from '@angular/core';
import { AchatService} from 'src/app/services/achat.service';
import {ActivatedRoute ,Router} from '@angular/router';
import {LoadingController , AlertController , ToastController} from '@ionic/angular';
import {FormControl , FormGroup , Validators} from '@angular/forms'

@Component({
  selector: 'app-add-achat',
  templateUrl: './add-achat.page.html',
  styleUrls: ['./add-achat.page.scss'],
})
export class AddAchatPage implements OnInit {
  listeAchat:any ; 
  nombreSac: number =0 ; 
  somme: number = 0 ; 

  constructor(
    private achatService: AchatService , 
    private loadingController: LoadingController, 
    private toastController: ToastController , 
    private route: ActivatedRoute , 
    private router: Router 

  ) { }

  ngOnInit() {
  }

  ionViewDidEnter(){
    this.informationPanier();
  }

  async informationPanier(){
    const loading = await this.loadingController.create({message: 'Loading...'});
    loading.present();
    this.achatService.informationPanier().subscribe(res => {
            this.listeAchat = res ; 
            this.somme=this.listeAchat.somme ; 
            this.nombreSac = this.listeAchat.sac ; 
            console.log(this.somme);
            loading.dismiss();
    });
  }

}
