import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddReaprovPage } from './add-reaprov.page';

const routes: Routes = [
  {
    path: '',
    component: AddReaprovPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddReaprovPageRoutingModule {}
