import { Component, OnInit } from '@angular/core';
import { ReaprovService} from 'src/app/services/reaprov.service';
import { ProduitService} from 'src/app/services/produit.service';
import {ActivatedRoute ,Router} from '@angular/router';

import {LoadingController , AlertController , ToastController} from '@ionic/angular';
import {FormControl , FormGroup , Validators} from '@angular/forms'

@Component({
  selector: 'app-add-reaprov',
  templateUrl: './add-reaprov.page.html',
  styleUrls: ['./add-reaprov.page.scss'],
})
export class AddReaprovPage implements OnInit {
  id:number=0;
  produitData : any ; 
  designation:string="";
  nomFrns: string ="";
  stock: number;
  formReaprov:FormGroup;

  constructor(
    private reaprovService: ReaprovService,
    private produitService: ProduitService,
    private loadingController: LoadingController, 
    private toastController: ToastController , 
    private route: ActivatedRoute , 
    private router: Router ) { }
    

  ngOnInit() {
    this.initialiseForm();
    if(this.route.snapshot.params.id){
      this.id=this.route.snapshot.params.id;
      this.getProduitById(this.id);
      
    };
  }


  //get valeur à modifier  
  async getProduitById(id:number){
    const loading = await this.loadingController.create({message: 'Loading...'});
    loading.present();
      this.produitService.getProduitById(id).subscribe( res=>{
          this.produitData = res;   
          this.designation = this.produitData.produit.designation ;
          this.nomFrns = this.produitData.produit.nomFrns; 
          this.stock = this.produitData.produit.quantite;     
          loading.dismiss();
      });  
  }

  initialiseForm(){
    this.formReaprov = new FormGroup({
        quantiteReap: new FormControl(null , [Validators.required]),
    });
  }

  reaproviser(){
    this.reaprovService.reaprovise(this.id , this.formReaprov.value).subscribe(res => {
        this.showAlert('Reaprovisionnement reussie');
        this.router.navigate(['/reaprov']);  
    });
  }

   //alert();
  async showAlert(MyMessage:string){
    const toast= await this.toastController.create({
      message: ''+MyMessage+'',
      duration: 4000,
      position: 'bottom', 
    });
    toast.present();
  }


}
