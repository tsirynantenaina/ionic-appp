import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddReaprovPageRoutingModule } from './add-reaprov-routing.module';

import { AddReaprovPage } from './add-reaprov.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    AddReaprovPageRoutingModule
  ],
  declarations: [AddReaprovPage]
})
export class AddReaprovPageModule {}
