import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReaprovPage } from './reaprov.page';

const routes: Routes = [
  {
    path: '',
    component: ReaprovPage
  },
  {
    path: 'add-reaprov',
    loadChildren: () => import('./add-reaprov/add-reaprov.module').then( m => m.AddReaprovPageModule)
  },
  {
    path: 'liste-reaprov',
    loadChildren: () => import('./liste-reaprov/liste-reaprov.module').then( m => m.ListeReaprovPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReaprovPageRoutingModule {}
