import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListeReaprovPage } from './liste-reaprov.page';

const routes: Routes = [
  {
    path: '',
    component: ListeReaprovPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListeReaprovPageRoutingModule {}
