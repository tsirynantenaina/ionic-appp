import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListeReaprovPageRoutingModule } from './liste-reaprov-routing.module';

import { ListeReaprovPage } from './liste-reaprov.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListeReaprovPageRoutingModule
  ],
  declarations: [ListeReaprovPage]
})
export class ListeReaprovPageModule {}
