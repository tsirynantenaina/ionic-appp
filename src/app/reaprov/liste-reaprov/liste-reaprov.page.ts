import { Component, OnInit } from '@angular/core';
import { ReaprovService} from 'src/app/services/reaprov.service';
import {LoadingController , ToastController} from '@ionic/angular';

@Component({
  selector: 'app-liste-reaprov',
  templateUrl: './liste-reaprov.page.html',
  styleUrls: ['./liste-reaprov.page.scss'],
})
export class ListeReaprovPage implements OnInit {
  listeReaprov:any ; 

  constructor(
    private reaprovService : ReaprovService, 
    private loadingCtrl:LoadingController ) { }

  ngOnInit() {
  }


  ionViewDidEnter(){
    this.allReaprov();
  }

  async allReaprov(){
    const loading = await this.loadingCtrl.create({message: 'Loading...'});
    loading.present();
    this.reaprovService.getAllReaprov().subscribe(
       res =>{this.listeReaprov = res ; loading.dismiss() ; }
    );
  }
}
