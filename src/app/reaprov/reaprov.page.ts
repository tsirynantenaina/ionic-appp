import { Component, OnInit } from '@angular/core';
import { ProduitService} from 'src/app/services/produit.service';
import {LoadingController , ToastController} from '@ionic/angular';

@Component({
  selector: 'app-reaprov',
  templateUrl: './reaprov.page.html',
  styleUrls: ['./reaprov.page.scss'],
})
export class ReaprovPage implements OnInit {
  listeProduit: any ; 
  constructor(
      private produitService: ProduitService,
      private loadingCtrl:LoadingController , 
      private toastController: ToastController 
  ) { }

  ngOnInit() {
  }

   ionViewDidEnter(){
    this.allProduit();
  }

  async allProduit(){
    const loading = await this.loadingCtrl.create({message: 'Loading...'});
    loading.present();
    this.produitService.getAllProduit().subscribe(
       res =>{this.listeProduit= res ; loading.dismiss() ; console.log(this.listeProduit)}
    );
  }

}
