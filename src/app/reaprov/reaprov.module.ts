import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReaprovPageRoutingModule } from './reaprov-routing.module';

import { ReaprovPage } from './reaprov.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReaprovPageRoutingModule
  ],
  declarations: [ReaprovPage]
})
export class ReaprovPageModule {}
