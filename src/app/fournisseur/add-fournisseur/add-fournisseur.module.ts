import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddFournisseurPageRoutingModule } from './add-fournisseur-routing.module';

import { AddFournisseurPage } from './add-fournisseur.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    AddFournisseurPageRoutingModule
  ],
  declarations: [AddFournisseurPage]
})
export class AddFournisseurPageModule {}
