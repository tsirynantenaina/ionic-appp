import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddFournisseurPage } from './add-fournisseur.page';

const routes: Routes = [
  {
    path: '',
    component: AddFournisseurPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddFournisseurPageRoutingModule {}
