import { Component, OnInit } from '@angular/core';
import {LoadingController , AlertController , ToastController} from '@ionic/angular';
import {Observable} from 'rxjs';

import {FournisseurService} from 'src/app/services/fournisseur.service';
import {Fournisseur} from 'src/app/entity/fournisseur';
import {ActivatedRoute ,Router} from '@angular/router';
import {FormControl , FormGroup , Validators} from '@angular/forms';

@Component({
  selector: 'app-add-fournisseur',
  templateUrl: './add-fournisseur.page.html',
  styleUrls: ['./add-fournisseur.page.scss'],
})
export class AddFournisseurPage implements OnInit {
  id:number=0;
  formFournisseur : FormGroup ; 
  fournisseurData:any ; 



  constructor(
    private fournisseurService : FournisseurService ,
    private toastController: ToastController , 
    private loadingController : LoadingController, 
    private route: ActivatedRoute , 
    private router: Router ){}

  ngOnInit() {
    this.initialiseForm();//initialiser la formulaire

    // si il y a un parametre id
    if(this.route.snapshot.params.id){
      this.id=this.route.snapshot.params.id;
      this.getFournisseurById(this.id);
    };
    
  }

  //initiliser formulaire
  initialiseForm(){
    this.formFournisseur = new FormGroup({
        nomFrns: new FormControl(null , [Validators.required]),
        adresseFrns: new FormControl(null , [Validators.required]),
        telFrns: new FormControl(null , [Validators.required]),
    });
  }

  //inserer 
  async addFournisseur(){   
   this.fournisseurService.addFournisseur(this.formFournisseur.value).subscribe( res =>{
      this.showAlert('insertion reussie');   
      this.formFournisseur.reset();

   });
   
      
  }



  //get valeur à modifier  
  async getFournisseurById(id:number){
    const loading = await this.loadingController.create({message: 'Loading...'});
    loading.present();
      this.fournisseurService.getFournisseurById(id).subscribe( res=>{
          this.fournisseurData = res;
          this.formFournisseur.setValue({
            nomFrns : this.fournisseurData.fournisseur.nomFrns ,
            adresseFrns : this.fournisseurData.fournisseur.adresseFrns , 
            telFrns : this.fournisseurData.fournisseur.telFrns ,
          });
          loading.dismiss();
      });  
  }


  //modification
  async updateFournisseur(){   
   this.fournisseurService.updateFournisseur(this.id , this.formFournisseur.value).subscribe( res =>{ 
      this.showAlert('Mise à jour reussie');
      this.router.navigate(['/fournisseur']);  
   });  
     
  }


  //alert();
  async showAlert(MyMessage:string){
    const toast= await this.toastController.create({
      message: ''+MyMessage+'',
      duration: 4000,
      position: 'bottom', 
    });
    toast.present();
  }

  

}
