import { Component, OnInit } from '@angular/core';
import {FournisseurService} from 'src/app/services/fournisseur.service';
import {Fournisseur} from 'src/app/entity/fournisseur';

import {LoadingController , ToastController} from '@ionic/angular';

@Component({
  selector: 'app-fournisseur',
  templateUrl: './fournisseur.page.html',
  styleUrls: ['./fournisseur.page.scss'],
})
export class FournisseurPage implements OnInit {
  listeFournisseur:any ; 

  constructor(
    private fournisseurService : FournisseurService , 
    private loadingCtrl:LoadingController , 
    private toastController: ToastController ) {
  
  }

  ngOnInit() {
  }
  
  ionViewDidEnter(){
    this.allFournisseur();
  }

  async allFournisseur(){
    const loading = await this.loadingCtrl.create({message: 'Loading...'});
    loading.present();
    this.fournisseurService.getAllFournisseur().subscribe(
       res =>{this.listeFournisseur= res ; loading.dismiss() ; }
    );
  }


  //suppression
  async deleteFournisseur(id:number){
    this.fournisseurService.deleteFournisseur(id).subscribe( res=> {
      this.allFournisseur(); 
      this.showAlert('Suppresion fournisseur reussie');
    })
  }

  //alert();
  async showAlert(MyMessage:string){
    const toast= await this.toastController.create({
      message: ''+MyMessage+'',
      duration: 4000,
      position: 'bottom', 
    });
    toast.present();
  }
 


}
