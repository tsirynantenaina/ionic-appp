import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FournisseurPageRoutingModule } from './fournisseur-routing.module';

import { FournisseurPage } from './fournisseur.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    FournisseurPageRoutingModule
  ],
  declarations: [FournisseurPage]
})
export class FournisseurPageModule {}
