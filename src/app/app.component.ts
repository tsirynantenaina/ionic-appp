import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Fournisseur', url: '/fournisseur', icon: 'archive' },
    { title: 'Produit', url: '/produit', icon: 'card' },
    { title: 'Client', url: '/client', icon: 'people' },
    { title: 'Provision', url: '/reaprov', icon: 'add' },
    { title: 'Achat', url: '/achat', icon: 'compass' },

   
  ];
  public labels = ['Parametre', 'Deconnexion'];
  constructor() {}
}
