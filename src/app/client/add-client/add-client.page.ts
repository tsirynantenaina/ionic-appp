import { Component, OnInit } from '@angular/core';
import {LoadingController , AlertController , ToastController} from '@ionic/angular';

import {ClientService} from 'src/app/services/client.service';
import {Client} from 'src/app/entity/client';
import {ActivatedRoute ,Router} from '@angular/router';
import {FormControl , FormGroup , Validators} from '@angular/forms';

@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.page.html',
  styleUrls: ['./add-client.page.scss'],
})
export class AddClientPage implements OnInit {
  id:number=0;
  formClient: FormGroup ; 
  clientData:any ;

  constructor(
    private clientService : ClientService ,
    private toastController: ToastController , 
    private route: ActivatedRoute , 
    private router: Router) { 
  }

  ngOnInit() {
    this.initialiseForm();//initialiser la formulaire

      // si il y a un parametre id
    if(this.route.snapshot.params.id){
      this.id=this.route.snapshot.params.id;
      this.getClientById(this.id);
    };
  }

  //initiliser formulaire
  initialiseForm(){
    this.formClient = new FormGroup({
        nomCli: new FormControl(null , [Validators.required]),
        adresseCli: new FormControl(null , [Validators.required]),
        telCli: new FormControl(null , [Validators.required]),
    });
  }

  //inserer 
  async addClient(){   
   this.clientService.addClient(this.formClient.value).subscribe( res =>{
      this.showAlert('insertion reussie');   
      this.formClient.reset();
   });   
  }

   //get valeur à modifier  
  getClientById(id:number){
      this.clientService.getClientById(id).subscribe( res=>{
          this.clientData = res;
          this.formClient.setValue({
            nomCli : this.clientData.client.nomCli ,
            adresseCli : this.clientData.client.adresseCli , 
            telCli : this.clientData.client.telCli ,
          });
      });  
  }


  //modification
  async updateClient(){   
   this.clientService.updateClient(this.id , this.formClient.value).subscribe( res =>{ 
      this.showAlert('Mise à jour reussie');
      this.router.navigate(['/client']);  
   });  
     
  }

     //alert();
  async showAlert(MyMessage:string){
    const toast= await this.toastController.create({
      message: ''+MyMessage+'',
      duration: 4000,
      position: 'bottom', 
    });
    toast.present();
  }

}
