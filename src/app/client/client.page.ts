import { Component, OnInit } from '@angular/core';
import { ClientService} from 'src/app/services/client.service';
import {LoadingController , ToastController} from '@ionic/angular';
@Component({
  selector: 'app-client',
  templateUrl: './client.page.html',
  styleUrls: ['./client.page.scss'],
})
export class ClientPage implements OnInit {
  listeClient:any ; 
  constructor(
    private clientService:ClientService, 
    private loadingCtrl:LoadingController , 
    private toastController: ToastController) { }

  ngOnInit() {
  }

  ionViewDidEnter(){
    this.allClient();
  }

  async allClient(){
    const loading = await this.loadingCtrl.create({message: 'Loading...'});
    loading.present();
    this.clientService.getAllClient().subscribe(
       res =>{this.listeClient= res ; loading.dismiss() ; }
    );
  }

  //suppression
  async deleteClient(id:number){
    this.clientService.deleteClient(id).subscribe( res=> {
      this.allClient(); 
      this.showAlert('Suppresion client reussie');
    })
  }

  //alert();
  async showAlert(MyMessage:string){
    const toast= await this.toastController.create({
      message: ''+MyMessage+'',
      duration: 4000,
      position: 'bottom', 
    });
    toast.present();
  }
}
