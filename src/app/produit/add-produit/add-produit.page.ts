import { Component, OnInit } from '@angular/core';
import { FournisseurService} from 'src/app/services/fournisseur.service';
import { ProduitService} from 'src/app/services/produit.service';
import {ActivatedRoute ,Router} from '@angular/router';

import {LoadingController , AlertController , ToastController} from '@ionic/angular';
import {FormControl , FormGroup , Validators} from '@angular/forms'

@Component({
  selector: 'app-add-produit',
  templateUrl: './add-produit.page.html',
  styleUrls: ['./add-produit.page.scss'],
})
export class AddProduitPage implements OnInit {
  listeFournisseur:any ; 
  id:number=0;
  formProduit:FormGroup; 
  produitData:any; 

  //fournisseur selectionner par defaut lors dela modification
  selectedFournisseur:string="" ; 


  constructor(
    private fournisseurService: FournisseurService,
    private produitService: ProduitService,
    private loadingController: LoadingController, 
    private toastController: ToastController , 
    private route: ActivatedRoute , 
    private router: Router) { 



  }

  ngOnInit() {
    this.initialiseForm();

    if(this.route.snapshot.params.id){
      this.id=this.route.snapshot.params.id;
      this.getProduitById(this.id);
       
    };
  }

  ionViewDidEnter(){
    this.allFournisseur();
    
  }

  initialiseForm(){
    this.formProduit = new FormGroup({

        designation: new FormControl(null , [Validators.required]),
        quantite: new FormControl(null , [Validators.required]),
        prix: new FormControl(null , [Validators.required]),
        idFrns: new FormControl(null , [Validators.required]),
    });
  }


  // get all fournisseur 
  async allFournisseur(){
    const loading = await this.loadingController.create({message: 'Loading...'});
    loading.present();
    this.fournisseurService.getAllFournisseur().subscribe(
       res =>{ this.listeFournisseur= res ; 
                //si il ya un parametre :id
                 if(this.id!==0){
                   this.formProduit.patchValue({
                      idFrns: ''+this.selectedFournisseur+'',
                    });
                 }

                loading.dismiss();
             }
     );
    
    
  }


  //add produit
  async addProduit(){   
   this.produitService.addProduit(this.formProduit.value).subscribe( res =>{
       this.showAlert('Insertion produit reussie');   
       this.formProduit.reset();
   });

   //console.log(this.formProduit.value);
  }

  //get valeur à modifier  
  getProduitById(id:number){
      this.produitService.getProduitById(id).subscribe( res=>{
          this.produitData = res;
          this.selectedFournisseur = this.produitData.produit.idFrns;
               this.formProduit.setValue({
               idFrns: this.produitData.produit.idFrns,
               designation:this.produitData.produit.designation,
               quantite:this.produitData.produit.quantite,
               prix:this.produitData.produit.prix,

            });
      });  
  }


  //modification
  async updateProduit(){   
   this.produitService.updateProduit(this.id , this.formProduit.value).subscribe( res =>{ 
      this.showAlert('Mise à jour reussie');
      this.router.navigate(['/produit']);  
   });  
     
  }

   //alert();
  async showAlert(MyMessage:string){
    const toast= await this.toastController.create({
      message: ''+MyMessage+'',
      duration: 4000,
      position: 'bottom', 
    });
    toast.present();
  }





}
