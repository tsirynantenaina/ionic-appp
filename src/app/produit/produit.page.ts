import { Component, OnInit } from '@angular/core';
import { ProduitService} from 'src/app/services/produit.service';
import {LoadingController , ToastController} from '@ionic/angular';


@Component({
  selector: 'app-produit',
  templateUrl: './produit.page.html',
  styleUrls: ['./produit.page.scss'],
})
export class ProduitPage implements OnInit {
  listeProduit: any ; 
  constructor(
      private produitService: ProduitService,
      private loadingCtrl:LoadingController , 
      private toastController: ToastController 
    ) {  }

  ngOnInit() {
  }


  ionViewDidEnter(){
    this.allProduit();
  }

  async allProduit(){
    const loading = await this.loadingCtrl.create({message: 'Loading...'});
    loading.present();
    this.produitService.getAllProduit().subscribe(
       res =>{this.listeProduit= res ; loading.dismiss() ; console.log(this.listeProduit)}
    );
  }


  //suppression
  async deleteProduit(id:number){
    this.produitService.deleteProduit(id).subscribe( res=> {
      this.allProduit(); 
      this.showAlert('Suppresion produit reussie');
    })
  }

  //alert();
  async showAlert(MyMessage:string){
    const toast= await this.toastController.create({
      message: ''+MyMessage+'',
      duration: 4000,
      position: 'middle', 
    });
    toast.present();
  }
}
