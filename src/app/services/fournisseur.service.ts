import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {Fournisseur} from '../entity/fournisseur';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})

export class FournisseurService {
  
  constructor(private http: HttpClient) { }


  getAllFournisseur():Observable<Fournisseur[]>
  {
    return this.http.get<Fournisseur[]>('http://localhost:8000/api/allFournisseur');
  }

   addFournisseur(fournisseur: Fournisseur):Observable<Fournisseur>
  {
    return this.http.post<Fournisseur>('http://localhost:8000/api/addFournisseur' ,fournisseur);
  }

   getFournisseurById(id:any):Observable<Fournisseur>
  {
    return this.http.get<Fournisseur>('http://localhost:8000/api/getFournisseur/'+id);
  }


   updateFournisseur(id:number , fournisseur: Fournisseur):Observable<Fournisseur>
  {
    return this.http.put<Fournisseur>('http://localhost:8000/api/updateFournisseur/'+id ,fournisseur);
  }


   deleteFournisseur(id:number):Observable<Fournisseur>
  {
    return this.http.delete<Fournisseur>('http://localhost:8000/api/deleteFournisseur/'+id);
  }
}