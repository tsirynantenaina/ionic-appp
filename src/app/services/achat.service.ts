import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {Achat} from 'src/app/entity/achat';
import {HttpClient} from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class AchatService {

  constructor(private http: HttpClient) { }

  addPanier(id:number , data: any)
  {
    return this.http.put('http://localhost:8000/api/addPanier/'+id , data );
  }

  informationPanier():Observable<Achat[]>
  {
    return this.http.get<Achat[]>('http://localhost:8000/api/dansPanier');
  }
}
