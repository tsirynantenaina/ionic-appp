import { Injectable } from '@angular/core';
import {Produit} from 'src/app/entity/produit';
import { Observable } from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProduitService {

  constructor(private http: HttpClient) { }

  getAllProduit():Observable<Produit[]>
  {
    return this.http.get<Produit[]>('http://localhost:8000/api/allProduit');
  }

 addProduit(produit: Produit):Observable<Produit>
  {
    return this.http.post<Produit>('http://localhost:8000/api/addProduit', produit);
  }

  getProduitById(id:number):Observable<Produit>
  {
    return this.http.get<Produit>('http://localhost:8000/api/getProduit/'+id);
  }


   updateProduit(id:number , produit: Produit):Observable<Produit>
  {
    return this.http.put<Produit>('http://localhost:8000/api/updateProduit/'+id ,produit);
  }


   deleteProduit(id:number):Observable<Produit>
  {
    return this.http.delete<Produit>('http://localhost:8000/api/deleteProduit/'+id);
  }
}
