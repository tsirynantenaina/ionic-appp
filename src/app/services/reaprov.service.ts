import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {Produit} from 'src/app/entity/produit';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ReaprovService {

  constructor(private http : HttpClient) { }

  reaprovise(id:number , produit: Produit):Observable<Produit>
  {
    return this.http.put<Produit>('http://localhost:8000/api/reaproviserProduit/'+id , produit );
  }

  getAllReaprov(){
    return this.http.get('http://localhost:8000/api/allReaprovise');
  }



}
