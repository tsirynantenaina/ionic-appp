import { Injectable } from '@angular/core';
import { Client } from 'src/app/entity/client';
import {Observable} from 'rxjs';
import { HttpClient} from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private http: HttpClient) { }


  getAllClient():Observable<Client[]>
  {
    return this.http.get<Client[]>('http://localhost:8000/api/allClient');
  }


   addClient(client: Client):Observable<Client>
  {
    return this.http.post<Client>('http://localhost:8000/api/addClient', client);
  }

   getClientById(id:number):Observable<Client>
  {
    return this.http.get<Client>('http://localhost:8000/api/getClient/'+id);
  }


   updateClient(id:number , client: Client):Observable<Client>
  {
    return this.http.put<Client>('http://localhost:8000/api/updateClient/'+id , client);
  }


   deleteClient(id:number):Observable<Client>
  {
    return this.http.delete<Client>('http://localhost:8000/api/deleteClient/'+id);
  }


}
