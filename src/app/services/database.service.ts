import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';

@Injectable({
  providedIn: 'root'
})

export class DatabaseService {
  
  private dbInstance: SQLiteObject;
  readonly db_name: string = "ionic_db";
  readonly db_table_fournisseur: string = "fournisseur";
  FOURNISSEURS: Array <any> ;

  constructor(
    private platform: Platform,
    private sqlite: SQLite    
  ) { 
    this.databaseConnexion();
  }

    // Create SQLite database 
    databaseConnexion() {
        this.platform.ready().then(() => {
          this.sqlite.create({
              name: this.db_name,
              location: 'default'
            }).then((sqLite: SQLiteObject) => {
              this.dbInstance = sqLite;
              sqLite.executeSql(`
                  CREATE TABLE IF NOT EXISTS ${this.db_table_fournisseur} (
                    idFrns INTEGER PRIMARY KEY, 
                    nomFrns varchar(100),
                    adresseFrns varchar(100),
                    telFrns varchar(15)
                  )`, []) 
                .then((res) => {
                  // alert(JSON.stringify(res));
                })
                .catch((error) => alert(JSON.stringify(error)));
            })
            .catch((error) => alert(JSON.stringify(error)));
        });   
    }

    // Ajout Fournisseur 
    public addFournisseur(nomFrns,adresseFrns , telFrns) {
      // validation
      if (!nomFrns.length || !adresseFrns.length || !telFrns.length) { 
        alert('Verifier les données saisis');
        return;
      }
      this.dbInstance.executeSql(`
      INSERT INTO ${this.db_table_fournisseur} (nomFrns, adresseFrns , telFrns) VALUES ('${nomFrns}', '${adresseFrns}' , '${telFrns}')`, [])
        .then(() => {
          alert("Success");
          this.getAllFournisseur();
        }, (e) => {
          alert(JSON.stringify(e.err));
        });
    }

    getAllFournisseur() {
      return this.dbInstance.executeSql(`SELECT * FROM ${this.db_table_fournisseur}`, []).then((res) => {
        this.FOURNISSEURS = [];
        if (res.rows.length > 0) {
          for (var i = 0; i < res.rows.length; i++) {
            this.FOURNISSEURS.push(res.rows.item(i));
          }
          return this.FOURNISSEURS;
        }
      },(e) => {
        alert(JSON.stringify(e));
      });
    }

    // Get fournisseur
    getFournisseur(id): Promise<any> {
      return this.dbInstance.executeSql(`SELECT * FROM ${this.db_table_fournisseur} WHERE idFrns = ?`, [id])
      .then((res) => { 
        return {
          idFrns: res.rows.item(0).idFrns,
          nomFrns: res.rows.item(0).nomFrns,  
          adresseFrns: res.rows.item(0).adresseFrns,
          telFrns: res.rows.item(0).telFrns
        }
      });
    }

    // Update fournisseur
    updateFournisseur(id, nomFrns, adresseFrns , telFrns) {
      let data = [nomFrns, adresseFrns , telFrns];
      return this.dbInstance.executeSql(`UPDATE ${this.db_table_fournisseur} SET nomFrns = ? , adresseFrns = ? , telFrns = ? WHERE idFrns = ${id}`, data)
    }  

    // Delete Fournisseur
    deleteFournisseur(id) {
      this.dbInstance.executeSql(`
      DELETE FROM ${this.db_table_fournisseur} WHERE idFrns = ${id}`, [])
        .then(() => {
          alert("Suppression réussie");
          this.getAllFournisseur();
        })
        .catch(e => {
          alert(JSON.stringify(e))
        });
    }

}