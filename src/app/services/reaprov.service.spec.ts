import { TestBed } from '@angular/core/testing';

import { ReaprovService } from './reaprov.service';

describe('ReaprovService', () => {
  let service: ReaprovService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ReaprovService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
