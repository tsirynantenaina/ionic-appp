import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'fournisseur',
    pathMatch: 'full'
  },
  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  },
  {
    path: 'fournisseur',
    loadChildren: () => import('./fournisseur/fournisseur.module').then( m => m.FournisseurPageModule)
  },
  {
    path: 'add-fournisseur',
    loadChildren: () => import('./fournisseur/add-fournisseur/add-fournisseur.module').then( m => m.AddFournisseurPageModule)
  },
  {
    path: 'update-fournisseur/:id',
        loadChildren: () => import('./fournisseur/add-fournisseur/add-fournisseur.module').then( m => m.AddFournisseurPageModule)
  },
  {
    path: 'produit',
    loadChildren: () => import('./produit/produit.module').then( m => m.ProduitPageModule)
  },
  {
    path: 'add-produit',
    loadChildren: () => import('./produit/add-produit/add-produit.module').then( m => m.AddProduitPageModule)
  },
  {
    path: 'update-produit/:id',
    loadChildren: () => import('./produit/add-produit/add-produit.module').then( m => m.AddProduitPageModule)
  },
  {
    path: 'client',
    loadChildren: () => import('./client/client.module').then( m => m.ClientPageModule)
  },
  {
    path: 'add-client',
    loadChildren: () => import('./client/add-client/add-client.module').then( m => m.AddClientPageModule)
  },
  {
    path: 'update-client/:id',
    loadChildren: () => import('./client/add-client/add-client.module').then( m => m.AddClientPageModule)
  },
  {
    path: 'reaprov',
    loadChildren: () => import('./reaprov/reaprov.module').then( m => m.ReaprovPageModule)
  }
  ,
  {
    path: 'add-reaprov/:id',
    loadChildren: () => import('./reaprov/add-reaprov/add-reaprov.module').then( m => m.AddReaprovPageModule)
  },
  {
    path: 'liste-reaprov',
    loadChildren: () => import('./reaprov/liste-reaprov/liste-reaprov.module').then( m => m.ListeReaprovPageModule)
  },
  {
    path: 'achat',
    loadChildren: () => import('./achat/achat.module').then( m => m.AchatPageModule)
  },
  {
    path: 'add-panier/:id',
    loadChildren: () => import('./achat/add-panier/add-panier.module').then( m => m.AddPanierPageModule)
  }
  ,
  {
    path: 'add-achat',
    loadChildren: () => import('./achat/add-achat/add-achat.module').then( m => m.AddAchatPageModule)
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
